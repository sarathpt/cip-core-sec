# CVE checks

This repository contains a wrapper for the Debian security analyzer `debsecan`.
The tool can be used to analyze a list of Debian packages (`/var/lib/dpkg/status`)
and report any found vulnerabilities in `.csv` or `.xlsx` format.
The tool also contains support for automation through Docker and gitlab-ci scripts.

# Docker image

Build the Docker image
```
host$ make build_docker_nocache
```

How to use the image
```
host$ make run_docker
docker$ cve-checker.py --suite buster --status ../dpkg-status/status.txt [--include-temporary]
```

Run the examples
```
host$ make run_docker_sample
host$ make run_docker_sample_bullseye
```

# Integration with Gitlab CI

This project contains a `.gitlab-ci.yml` file that you can use as a reference for setting up your own instance of `cip-core-sec` and use it in your projects.

The `.gitlab-ci.yml` file shows how to automate the build and registration of the `cip-core-sec` Docker image. If you prefer to do the registration manually, you can follow the next steps.
```
$ docker login registry.gitlab.com -u myuser -p mytoken
$ docker tag cip-core-sec/cve-checker:latest registry.gitlab.com/cip-project/cip-core/cip-core-sec/cve-checker:latest
$ docker push registry.gitlab.com/cip-project/cip-core/cip-core-sec/cve-checker:latest
```

# License

This code is distributed under the Apache 2.0 license.
