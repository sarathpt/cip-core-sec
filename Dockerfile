# Copyright (c) 2019 TOSHIBA Corporation
# SPDX-License-Identifier: Apache-2.0
FROM debian:buster-slim

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y python3 python3-yaml python3-wget debsecan python3-xlsxwriter

COPY src/cve-checker.py /usr/local/bin
