build_docker:
	docker build -f Dockerfile -t cip-core-sec/cve-checker:latest .

build_docker_nocache:
	docker build --no-cache -f Dockerfile -t cip-core-sec/cve-checker:latest .

rm_docker:
	docker image rm cip-core-sec/cve-checker:latest

run_docker:
	if [ -z "$(shell docker images -q cip-core-sec/cve-checker:latest)" ]; then make build_docker; fi
	docker run --rm -it -v $(shell pwd):/cve-checker --workdir /cve-checker -u $(shell id -u ${USER}):$(shell id -g ${USER}) \
	cip-core-sec/cve-checker:latest

run_docker_sample:
	if [ -z "$(shell docker images -q cip-core-sec/cve-checker:latest)" ]; then make build_docker; fi
	docker run --rm -it -v $(shell pwd):/cve-checker --workdir /cve-checker -u $(shell id -u ${USER}):$(shell id -g ${USER}) \
	cip-core-sec/cve-checker:latest /bin/bash -c "cve-checker.py --include-temporary --suite buster --status ./dpkg-status/status.txt"

run_docker_sample_bullseye:
	if [ -z "$(shell docker images -q cip-core-sec/cve-checker:latest)" ]; then make build_docker; fi
	docker run --rm -it -v $(shell pwd):/cve-checker --workdir /cve-checker -u $(shell id -u ${USER}):$(shell id -g ${USER}) \
	cip-core-sec/cve-checker:latest /bin/bash -c "cve-checker.py --include-temporary --suite bullseye --status ./dpkg-status/status.txt"